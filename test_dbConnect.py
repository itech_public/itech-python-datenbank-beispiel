from unittest import TestCase
from connect_db import DbConnect

"""Dieser Test tested die Verbindung zur Datenbank"""

user = 'python'
pw = 'python'
host = 'localhost'
port = '3316'
database = 'python'

class TestDbConnect(TestCase):

    def test_connectDb(self):
        db = DbConnect(user, pw, host, port, database)
        db.connectDb()
        db.closeDb()

    def test_select_user_table(self):
        db = DbConnect(user, pw, host, port, database)
        cnx = db.connectDb()
        cursor = cnx.cursor()
        sql = "select * from user"
        cursor.execute(sql)
        rs = cursor.fetchall()
        print("Ergebnis {}".format(rs))

    def test_select_user_talbe_names_with_loop(self):
        db = DbConnect(user, pw, host, port, database)
        cnx = db.connectDb()
        cursor = cnx.cursor()
        sql = "select * from user"
        cursor.execute(sql)
        rs = cursor.fetchall()
        for row in rs:
            print ("Name Tabelle User {}".format(row[1]))


