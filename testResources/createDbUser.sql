SET AUTOCOMMIT = 0;

DROP USER 'python'@'%';

CREATE USER 'python'@'%' IDENTIFIED BY 'python';

GRANT ALL ON python.* TO 'python'@'%';

FLUSH PRIVILEGES;

COMMIT;

